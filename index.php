<?php
require_once __DIR__ . '/vendor/autoload.php';

$app = new \Klein\Klein();

$app->respond('/', function () {
    return 5 + 5;
});

$app->respond('/login', function ($request, $response, $service) {
    $service->pageTitle = 'Testttt';
    $service->render('views/login.php');
});

$app->dispatch();